#include "bmp.h"
#include <malloc.h>
#include <stdio.h>

void openFile(FILE** in, FILE** in2,const char *name, const char *name2) {
    *in=fopen(name, "rb");
    *in2=fopen(name2, "wb");
}

void close(FILE* in, FILE* in2) {
    fclose(in);
    fclose(in2);
}

