#include "bmp.h"
#include "fileOpener.h"

uint32_t fileSize(uint32_t height, uint32_t width){
        uint8_t padding=4-(width*3)%4;
        return (width*+padding)*height;
}

enum read_status from_bmp( FILE* in, struct image *img ){
        size_t readSize;
        uint8_t padding;
        header header={0};
        readSize=fread(&header,sizeof(header),1,in);
        if(readSize!=1)return READ_INVALID_HEADER;
        if (header.bfType != 0x4d42)return READ_INVALID_SIGNATURE; 
        padding=4-(header.biWidth*3)%4;
        data *dp = malloc(sizeof(data) * header.biWidth * header.biHeight);
        *img =(struct image) {header.biWidth,header.biHeight,dp};
        for (size_t i=header.biHeight;i!=0;i--) {
                readSize=fread(img->data+(i-1)*header.biWidth, sizeof(data)*header.biWidth, 1, in);
                if(readSize!=1){
                        free(dp);
                        return READ_INVALID_BITS;
                }
                fseek(in, +padding, SEEK_CUR);
        }
        return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
        uint8_t padding=4-(img->width*3)%4;
        //все значения взяты из wiki
        size_t dataSize=fileSize(img->height,img->width);
        int16_t litEnd=0x4d42;//const
        //int16_t bigeEnd=0x424d;
        int32_t bfileSize=(int32_t) dataSize+54;//54-const info bmp size
        int32_t biSize=40;//const
        uint16_t biPlanes=1;//const
        uint16_t bitCount=24;//rgb, каждый по байту
        uint32_t ppr=2834;//const
        header head={litEnd,bfileSize,0,54,biSize,img->width,img->height,biPlanes,bitCount,0,dataSize,ppr,ppr,0,0};
        size_t writeSize=fwrite(&head,sizeof(header),1,out);
        if (writeSize != 1) return WRITE_ERROR;
         for (size_t i = head.biHeight; i != 0; i--) {
                writeSize = fwrite(img->data + (i - 1) * head.biWidth,sizeof(struct pixel) * head.biWidth, 1, out);
                if (writeSize != 1) return WRITE_ERROR; 
                fseek(out, +padding, SEEK_CUR);
        }
        return WRITE_OK;
}

