#include "bmp.h"

struct image rotate(struct image const source) {
    data *rot = malloc(sizeof(data) * source.width * source.height);
    struct image rotImg ={source.height, source.width, rot};
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rot[(source.width - j - 1) * source.height + i]=source.data[i * source.width + j];
        }
    }
    return rotImg;
}
