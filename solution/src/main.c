#include "bmp.h"
#include "fileOpener.h"
#include "rotate.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

int main(int argc, char **argv){
    if(argc<3) {
        printf("неверные аргументы");
        return 1;
        }
    FILE *inFile, *outFile;
    openFile(&inFile, &outFile,argv[1], argv[2]);
    struct image image={0};
    enum read_status read=from_bmp(inFile,&image);
    if(read!=READ_OK){
        free(image.data);
        close(inFile,outFile);
        printf("ошибка чтения картинки");
        return 1;
    }
    struct image rotateImg=rotate(image);
    enum write_status write=to_bmp(outFile,&rotateImg);
    free(image.data);
    free(rotateImg.data);
    close(inFile,outFile);
    if(write!=WRITE_OK){
        printf("ошибка записи картинки");
        return 1;
    }
    return 0;
}

